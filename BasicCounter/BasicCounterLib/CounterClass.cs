﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasicCounterLib
{
    public class CounterClass 
    {
        public static int nbCounter=0;

        public int GetCounter()
        {
            return nbCounter;
        }

        public void Increment()
        {
            nbCounter++;
        }
        public void Decrement()
        {
            nbCounter--;
        }
        public void RAZ()
        {
            nbCounter=0;
        }
    }
}
