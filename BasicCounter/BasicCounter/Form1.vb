﻿Imports BasicCounterLib

Public Class Form1
    Dim nbCounter As New CounterClass()
    Private Sub plus_Click(sender As Object, e As EventArgs) Handles plus.Click
        nbCounter.Increment()
        Counter.Text = nbCounter.GetCounter
    End Sub

    Private Sub reset_Click(sender As Object, e As EventArgs) Handles reset.Click
        nbCounter.RAZ()
        Counter.Text = nbCounter.GetCounter
    End Sub

    Private Sub moins_Click(sender As Object, e As EventArgs) Handles moins.Click
        nbCounter.Decrement()
        Counter.Text = nbCounter.GetCounter
    End Sub
End Class
