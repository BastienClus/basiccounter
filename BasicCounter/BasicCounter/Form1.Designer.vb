﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.plus = New System.Windows.Forms.Button()
        Me.moins = New System.Windows.Forms.Button()
        Me.reset = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Counter = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'plus
        '
        Me.plus.Location = New System.Drawing.Point(534, 202)
        Me.plus.Name = "plus"
        Me.plus.Size = New System.Drawing.Size(75, 23)
        Me.plus.TabIndex = 0
        Me.plus.Text = "+"
        Me.plus.UseVisualStyleBackColor = True
        '
        'moins
        '
        Me.moins.Location = New System.Drawing.Point(199, 202)
        Me.moins.Name = "moins"
        Me.moins.Size = New System.Drawing.Size(75, 23)
        Me.moins.TabIndex = 1
        Me.moins.Text = "-"
        Me.moins.UseVisualStyleBackColor = True
        '
        'reset
        '
        Me.reset.Location = New System.Drawing.Point(357, 306)
        Me.reset.Name = "reset"
        Me.reset.Size = New System.Drawing.Size(75, 23)
        Me.reset.TabIndex = 2
        Me.reset.Text = "RAZ"
        Me.reset.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(357, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 17)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total"
        '
        'Counter
        '
        Me.Counter.AutoSize = True
        Me.Counter.Location = New System.Drawing.Point(370, 134)
        Me.Counter.Name = "Counter"
        Me.Counter.Size = New System.Drawing.Size(16, 17)
        Me.Counter.TabIndex = 4
        Me.Counter.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Counter)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.reset)
        Me.Controls.Add(Me.moins)
        Me.Controls.Add(Me.plus)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents plus As Button
    Friend WithEvents moins As Button
    Friend WithEvents reset As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Counter As Label
End Class
