﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterLib;

namespace BasicCounterTest 
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestGetcounter()
        {
            CounterClass counter = new CounterClass();
            
            Assert.AreEqual(0, counter.GetCounter());
        }
        [TestMethod]
        public void TestIncrement()
        {
            CounterClass counter = new CounterClass();
            counter.Increment(); 
            Assert.AreEqual(1, counter.GetCounter());
        }
        [TestMethod]
        public void TestDecrement()
        {
            CounterClass counterDecrement = new CounterClass();
            counterDecrement.Decrement();
            Assert.AreEqual(0, counterDecrement.GetCounter());
        }
        [TestMethod]
        public void TestRAZ()
        {
            
            CounterClass counter = new CounterClass();
            counter.Increment();
            counter.RAZ();
            Assert.AreEqual(0, counter.GetCounter());
        }
        
    }
}
